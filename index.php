<?php
session_start();
$titre = "FrozenBubble | WC-Game";
include 'header.inc.php';
include 'menu.inc.php';
include 'message.php';

if (isset($_GET["msg"])=='success') {
  # code...
  $value = $_SESSION['success'];
    echo "<div class='alert alert-primary alert-dismissible fade show' role='alert'>";
    echo "<strong>";
    echo $value;
    echo '</strong>';
    echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>";
    echo "</button>";
    echo "</div>";
}
?>
<!--Banner section-->
<div class="container mx-auto mt-2">
  <div class="text-center ">
    <h1>Bienvenue sur la plateforme de jeu <span class="log">FrozenBubble</span></h1>
    <img src="frozen.png" alt=""><br><br>
    <p>FrozenBubble est un jeu de puzzle classique qui défie les joueurs avec des niveaux de difficulté croissants. Le but est de tirer des bulles de couleur pour créer des groupes de trois ou plus de la même couleur et les faire disparaître de l'écran. Le jeu offre des graphismes colorés et des effets sonores accrocheurs, ainsi que des modes de jeu solo ou multijoueur!</p>
  </div>
</div>
<div class="container text-center">
  <h3 style="font-family: 'Jost', sans-serif;"> TEAM <span class="log">PORTUGAL</span></h3>
  <img class="img-fluid" src="Animated-Flag-Portugal.gif" alt=""><br><br>
</div>
<?php 
  include 'footer.inc.php';
?>