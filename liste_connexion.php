<?php
$titre = "Accueil | Organisateur";
include 'header.inc.php';
include 'menu_organisateur.inc.php';
// Configuration des informations de connexion SSH
$sshHosts = array(
    '195.221.60.132',
    '195.221.60.135'
);
$sshUsername = 'pi';
$sshPassword = 'Portugal';

// Fonction pour exécuter la commande SSH
function executeSshCommand($host, $username, $password, $command)
{
    $ssh = ssh2_connect($host);
    if (ssh2_auth_password($ssh, $username, $password)) {
        $stream = ssh2_exec($ssh, $command);
        stream_set_blocking($stream, true);
        $output = stream_get_contents($stream);
        fclose($stream);
        return $output;
    } else {
        return false;
    }
}

// Récupération des utilisateurs connectés pour chaque adresse IP
$connectedUsers = array();
foreach ($sshHosts as $host) {
    $command = 'who';
    $output = executeSshCommand($host, $sshUsername, $sshPassword, $command);
    if ($output !== false) {
        $users = explode("\n", trim($output));
        $connectedUsers[$host] = $users;
    }
}

// Affichage des utilisateurs connectés avec les boutons de déconnexion
foreach ($connectedUsers as $host => $users) {
    echo "<h3>Utilisateurs connectés sur ";

    if ($host == "195.221.60.132") {
        echo "Rasberry1";
    } elseif ($host == "195.221.60.135") {
        echo "Rasberry2";
    } else {
        echo $host;
    }

    echo " :</h3>";
    echo "<ul>";
    foreach ($users as $user) {
        $userInfo = explode(" ", $user);
        $usernamewho = $userInfo[0];
        $tty = $userInfo[1];
        echo "<li>$usernamewho ($tty) ";
        echo "<form method='post' action='disconnect.php'>";
        echo "<input type='hidden' name='host' value='$host'>";
        echo "<input type='hidden' name='username' value='$usernamewho'>";
        echo "<input type='hidden' name='tty' value='$tty'>";
        echo "<input type='submit' value='Déconnexion'>";
        echo "</form>";
        echo "</li>";
    }
    echo "</ul>";
}
?>
