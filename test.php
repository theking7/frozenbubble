<?php
error_reporting(E_ALL); 
ini_set("display_errors", 1);
// Active Directory server details
$adServer = 'ldaps://192.168.6.10:636'; // Replace with your AD server address

// AD server credentials
$ldapUsername = 'franck'; // Replace with your AD username
$ldapPassword = 'Password12'; // Replace with your AD password

// Connect to Active Directory server
$ldapConnection = ldap_connect($adServer);

ldap_set_option($ldapConnection, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_NEVER);
ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ldapConnection, LDAP_OPT_REFERRALS, 0);

if ($ldapConnection) {
    // Binding to AD server using the provided credentials
    $ldapBind = ldap_bind($ldapConnection, $ldapUsername, $ldapPassword);

    if ($ldapBind) {
        echo "LDAP bind successful!";
        
        // Perform LDAP operations here
        
        // Unbind from the AD server
        ldap_unbind($ldapConnection);
    } else {
        echo "LDAP bind failed!";
    }
} else {
    echo "Unable to connect to LDAP server!";
}