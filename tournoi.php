<?php
session_start();
 
 include 'header.inc.php';
 include 'menu_joueur.inc.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>jQuery Bracket</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-bracket/dist/jquery.bracket.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-bracket/dist/jquery.bracket.min.css">
</head>

<body>
<div id="tournament"></div>
<button id="add-player">Ajouter un joueur</button>
<button id="shuffle-teams">Mélanger les équipes</button>
<button id="generate-matches">Générer les matchs</button>
<script>
var tournamentData = {
  teams: [
    ["Team 1", "Team 2"],
    ["Team 3", "Team 4"],
    ["Team 5", "Team 6"],
    ["Team 7", "Team 8"]
  ]
};

    $(function() {
        $('#tournament').bracket({
            init: tournamentData
        });
    });

    function launchGame(index) {
        tournamentData.matches[index].started = true;
        $('#tournament').bracket({
            init: tournamentData
        });
    }

    function generateBracket() {
        var matches = [];
        for (var i = 0; i < tournamentData.teams.length; i += 2) {
            var match = [tournamentData.teams[i], tournamentData.teams[i + 1]];
            matches.push(match);
        }

        tournamentData.matches = matches.map(function(match) {
            return {
                teams: match,
                started: false
            };
        });

        $('#tournament').bracket({
            init: tournamentData
        });

        // Supprimer les anciens boutons de partie
        $('#match-buttons').empty();

        // Création des boutons "Lancer la partie"
        for (var j = 0; j < tournamentData.matches.length; j++) {
            var matchIndex = j;

            // Création du bouton "Lancer la partie"
            var launchButton = $('<button>')
                .text('Lancer la partie n° ' + (j + 1))
                .addClass('launch-button')
                .attr('data-index', matchIndex)
                .click(function() {
                    var index = $(this).attr('data-index');
                    launchGame(index);
                });

            $('#match-buttons').append(launchButton);
        }
    }

    function shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    $('#add-player').click(function() {
        tournamentData.teams.push(["Nouveau joueur"]);
        $('#tournament').bracket({
            init: tournamentData
        });
    });

    $('#shuffle-teams').click(function() {
        shuffle(tournamentData.teams);
        $('#tournament').bracket({
            init: tournamentData
        });
    });

    $('#generate-matches').click(function() {
        shuffle(tournamentData.teams);
        generateBracket();
    });
   


    </script>

<script src='tournoi.js'></script>
</body>

</html>