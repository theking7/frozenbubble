<?php
session_start();
$titre = "Inscription | WC-Game";
include 'header.inc.php';
include 'menu_1.php';
include 'message.php';
?>
<h2 class="text-center"> CREER UN COMPTE </h2>
<div class="container  d-flex justify-content-center">
<form class="needs-validation" action="tt_creation.php" method="POST">
    <div class="form-group was-validated">
        <label class="form-label" for="prenom">Prénom</label>
        <input class="form-control" type="le_prenom" id="prenom" required name="le_prenom">
    </div>
    <div class="form-group was-validated">
        <label class="form-label" for="user">Nom d'utilisateur</label>
        <input class="form-control" type="user" id="user" required name="le_user">
    </div>
    <div class="form-group was-validated">
        <label class="form-label" for="password">Mot de passe</label>
        <input class="form-control" type="password" id="password" required name="le_pass">
    </div>
    <div class="form-group was-validated">
     <label for="validationServer04" class="form-label">Utilisateur</label>
     <select class="form-select is-invalid" id="validationServer04" aria-describedby="validationServer04Feedback" required name="selectedOption">
        <option selected disabled value="">Utilisateur...</option>
        <option value="joueur">joueur</option>  
    </select>
    </div>
    <br><br><br>
    <input class="btn btn-danger w-100" name="submit" type="submit" value="VALIDER" >
</form>
<br><br><br>
</div>
</div>
