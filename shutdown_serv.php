<?php
include 'menu_organisateur.inc.php';
include 'header.inc.php';
// Configuration du service SSH 
$sshHost = '195.221.60.129';
$sshPort = 22;
$sshUser = 'portugal';
$sshPassword = 'portugal';

// Command pour démarrer un simple serveur Apache
$startCommand = 'killall fb-server';

// Function qui permet l'exécution de sudo
function executeSshCommandWithSudo($host, $port, $user, $password, $command) {
    // Connexion à SSH
    #$connection = ssh2_connect($host, $port);
    $connection = ssh2_connect($host,$port);

    if (!$connection) {
        die('Failed to connect to SSH');
    }
    // Authentification avec SSH
    if (!ssh2_auth_password($connection, $user, $password)) {
        die('SSH authentication failed');
    }
    // Démarrer un terminal
    $shell = ssh2_shell($connection, 'xterm');
    if (!$shell) {
        die('Failed to start shell');
    }
    // Envoi de la commandecsudo au shell
    fwrite($shell, "sudo su\n");
    // Attendre la demande du passwd
    usleep(100000); // Temps d'attente de 100 milliseconds
    // Envoi du password sudo au shell
    fwrite($shell, "$password\n");
    // Attendre la validation de la saise
    usleep(100000); // Wait for 100 milliseconds
    // Executer la commande voulue
    fwrite($shell, "$command\n");
    // Attendre la fin de l'exécution de la commande
    usleep(500000); // Wait for 500 milliseconds
    // Lecture de la sortie de cette commande
    $output = '';
    while ($line = fgets($shell)) {
        $output .= $line;
    }
    // Fermeture de la connection au SSH 
    ssh2_disconnect($connection);
    return $output;
}
// Execution de la commande avec sudo
$output = executeSshCommandWithSudo($sshHost, $sshPort, $sshUser, $sshPassword, $startCommand);
// Output the result
?>
<!DOCTYPE html>
<html>
<body>
<div class="container text-center">
        <h1>Serveur de jeu fermé avec succès .</h1>
        <img src="success.gif" alt="">
    </div>
</body>
</html>