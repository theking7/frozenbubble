<?php
$server = "195.221.60.135"; // server IP/hostname of the SSH server
$username = "pi"; // username for the user you are connecting as on the SSH server
$password = "Portugal"; // password for the user you are connecting as on the SSH server
$command = "export DISPLAY=:0; xhost +local:; /etc/start_qjoypad.sh"; // could be anything available on the server you are SSH'ing to

// Establish a connection to the SSH Server. Port is the second param.
$connection = ssh2_connect($server, 22);

// Authenticate with the SSH server
ssh2_auth_password($connection, $username, $password);

// Execute a command on the connected server and capture the response
$stream = ssh2_exec($connection, $command);

// Sets blocking mode on the stream
stream_set_blocking($stream, true);

// Get the response of the executed command in a human readable form
$output = stream_get_contents($stream);
?>