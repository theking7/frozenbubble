<?php
session_start();
$titre = "Connexion";
include 'header.inc.php';
include 'menu_erreur.php';
include 'msg_error.php';
?>
<div class="container text-center">
    <h1>Accès non autorisé</h1>
    <p>Désolé, vous n'êtes pas autorisé à accéder à cette page.</p>
    <p>Veuillez vous connecter avec un compte valide pour accéder au contenu.</p>
    <img src="warn.gif" alt="">
</div>
<br><br><br><br><br>
<?php
include 'footer.inc.php';
?>