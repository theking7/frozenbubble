<?php
session_start();
if (!isset($_SESSION['auth']['organisateur']) || $_SESSION['auth']['organisateur'] !== true) {
    // Rediriger vers une page d'erreur d'accès non autorisé
    header('Location: erreur.php');
    exit;
  }

  else {
    # code...
    include 'menu_organisateur.inc.php';
include 'header.inc.php';
// Configuration des informations de connexion SSH
$sshHost = $_POST['host'];
$sshUsername = 'pi';
$sshPassword = 'Portugal';
$sshPort = 22;
$username = $_POST['username'];
$tty = $_POST['tty'];

function executeSshCommandWithSudo($host, $port, $user, $password, $command) {
    // Connexion à SSH
    $connection = ssh2_connect($host, $port);

    if (!$connection) {
        die('Failed to connect to SSH');
    }
    // Authentification avec SSH
    if (!ssh2_auth_password($connection, $user, $password)) {
        die('SSH authentication failed');
    }
    // Exécution de la commande avec sudo
    $stream = ssh2_exec($connection, "sudo $command");
    stream_set_blocking($stream, true);
    $output = stream_get_contents($stream);
    fclose($stream);
    // Fermeture de la connexion SSH
    ssh2_disconnect($connection);
    return $output;
}

// Commande pour tuer le processus "frozen-bubble" en tant que superutilisateur
$command = "pkill -1 -U $username";
echo $tty;

// Exécution de la commande SSH en tant que superutilisateur
$output = executeSshCommandWithSudo($sshHost, $sshPort, $sshUsername, $sshPassword, $command);

echo $output;

if ($output !== false) {
    echo "Commande exécutée avec succès en tant que superutilisateur.";
} else {
    echo "Échec de l'exécution de la commande en tant que superutilisateur.";
}

}
?>
