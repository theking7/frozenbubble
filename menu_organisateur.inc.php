<header class="header">
    <nav  class="navbar navbar-expand-lg navbar-dark bg-dark fixed">
      <div class="container-fluid">
        <a class="navbar-brand " href="acceuil_organisateur.php">FROZEN<span class="logo-sp log">BUBBLE</span></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-lg-end px-5" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
          <li id="menu-dashboard" class="nav-item active">
                    <a class="nav-link active" aria-current="page" href="liste_connexion.php">Liste des joueurs connectés</a>
                </li>
                <li id="menu-dashboard" class="nav-item active">
                    <a class="nav-link active" aria-current="page" href="etat_serv.php">Etat des serveurs de jeu</a>
                </li>
                <li id="menu-dashboard" class="nav-item active">
                    <a class="nav-link active" aria-current="page" href="server.php">Lancer le serveur de jeu </a>
                </li>
                <li id="menu-dashboard" class="nav-item active">
                    <a class="nav-link active" aria-current="page" href="arborescence.php">Programmer un match </a>
                </li>
                <li id="menu-dashboard" class="nav-item active">
                    <a class="nav-link active" aria-current="page" href="decon.php">Déconnexion</a>
                </li>
          </ul>
        </div>
      </div>
    </nav>
</header>