<?php
$titre = "Arborescence de tournoi | WC-Game";
include 'header_arbo.php';
include 'menu_organisateur.inc.php';
?>

<div class="container mt-5 mx-auto text-center">
    <h2>Programmation des matchs</h2>
    <input class="form-control-sm" type="text" id="participant" placeholder="Ex : Alex">
    <button class="btn btn-danger" id="ajouter">Ajouter</button><br><br>
    <button class="btn btn-danger" id="vider">Vider</button>
    <button class="btn btn-danger" id="aleatoire">Mélanger</button>
    <button class="btn btn-danger" id="enregistrer">Sauvegarder</button>

	<style>
    .btn-group form {
        display: inline-block;
        margin-right: 10px;
    }
</style>

<div class="container mx-auto text-center mt-2">
    <h2>Gestion des matchs</h2>

    <div class="btn-group" role="group">
        <form method="POST" action="config_mannette_classique.php">
            <input type="submit" name="conf_class" value="Configuration classique" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="config_mannette_classique2.php">
            <input type="submit" name="conf_class" value="Configuration classique 2" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="config_mannette_inverse.php">
            <input type="submit" name="conf_inv" value="Configuration inversée" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="config_mannette_inverse2.php">
            <input type="submit" name="conf_class" value="Configuration inversée 2" class="btn btn-danger mt-2">
        </form>
    </div>

    <div class="btn-group" role="group">
        <form method="POST" action="lancerPartie1.php">
            <input type="submit" name="conf_inv" value="Lancer partie 1" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="arreterPartie1.php">
            <input type="submit" name="conf_inv" value="Arreter partie 1" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="lancerPartie2.php">
            <input type="submit" name="conf_inv" value="Lancer partie 2" class="btn btn-danger mt-2">
        </form>

        <form method="POST" action="arreterPartie2.php">
            <input type="submit" name="conf_inv" value="Arreter partie 2" class="btn btn-danger mt-2">
        </form>
    </div>
</div>
    	
</div>

<div id="tournoi"></div>

<br><br><br><br><br><br><br><br><br><br><br>

<?php
include 'footer.inc.php';
?>