<?php
session_start();
// Vérifier si l'utilisateur est authentifié
if (!isset($_SESSION['auth']['organisateur']) || $_SESSION['auth']['organisateur'] !== true) {
  // Rediriger vers une page d'erreur d'accès non autorisé
  header('Location: erreur.php');
  exit;
}
else{
    echo '<?php'; 
    $titre = "Accueil | Organisateur";
       include 'header.inc.php';
       include 'menu_organisateur.inc.php';
       include 'message.php';
    echo "<div class='container mx-auto text-center'>";
    echo "Bienvenue sur votre espace ". "<strong>" . $_SESSION['tonNom'] . "</strong>";
    echo "</div>";  
    echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
}
  include 'footer.inc.php';
?>