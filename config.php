<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if(isset($_POST['username']) && isset($_POST['pass'])){
    // Paramètre de configuration du serveur AD 
    #$adServer = "ldaps://dc1.frozen.pt";
    $adServer = "ldap://195.221.60.5";
    $ldap = ldap_connect($adServer);
    $username = $_POST['username'];
    $password = $_POST['pass'];
    //Options de protocole LDAP
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
    ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_ALLOW);
    ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);
    // Authentification avec l' AD
    $userConnect = $username.'@frozen.pt'; #element de l'authentification
    $bind = ldap_bind($ldap, $userConnect, $password);
    //Test
    if ($bind) {
        $filter="(samaccountname=$username)";
        $result = ldap_search($ldap,"dc=frozen,dc=pt",$filter);
        $info = ldap_get_entries($ldap, $result);

        for ($i=0; $i<$info["count"]; $i++)
        {
            if ($username == $info[$i]['samaccountname'][0])
            {
                $type = $info[$i]["description"][0];
                if ($type == "admin")
                {
                    $_SESSION['auth']['admin']=true;
                    $_SESSION['tonNom'] = $info[$i]["cn"][0];
                    $_SESSION['msg'] = "<p> <strong> Authentification au compte " .$type . " réussie </strong> </p>";
                    header('Location: acceuil_admin.php');
                }
                if ($type == "joueur")
                {
                    $_SESSION['auth']['joueur']=true;
                    $_SESSION['tonNom'] = $info[$i]["cn"][0];
                    $_SESSION['msg'] = "<p> <strong> Authentification au compte " .$type . " réussie </strong> </p>";
                    header('Location: acceuil_joueur.php');
                }
                if ($type == "organisateur")
                {
                    $_SESSION['auth']['organisateur']=true;
                    $_SESSION['tonNom'] = $info[$i]["cn"][0];
                    $_SESSION['msg'] = "<p> <strong> Authentification au compte " .$type . " réussie </strong> </p>";
                    header('Location: acceuil_organisateur.php');
                }
            }
        }
    }
    else 
    {
        header("Location: connexion.php?test=id");
    }
}
?>
    