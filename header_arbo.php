<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title><?php echo $titre ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="cssFiles/jquery.bracket.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="main.css">
    <script type="text/javascript" src="jsFiles/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="jsFiles/jquery.bracket.min.js"></script>
    <script type="text/javascript" src="jsFiles/tournoi.js"></script>
</head>
<body>