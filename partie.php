<?php
session_start();
 $titre = "Lancer une partie | WC-Game";
 include 'header.inc.php';
 include 'menu_organisateur.inc.php';
?>

<div class="container">
    <div class="text-center ">
       <br><br><br>
        <div class="row">
        <h3 class="mb-5">CHOIX DU MODE DE JEU </h3>

           <div class="col-md-4">
              <div class="card" style="margin-bottom :10px;">
                <div class="card-header">
                  <h4 class="text-center">Mode 1 joueur</h4>
                </div>
                <div class="card-body">
                  <!-- Contenu de la carte pour le mode 1 joueur -->
                   <img src="a.png" alt="">
                </div>
            </div>
            <a href="#"><button type="button" class="btn btn-outline-danger mb-2" >Démarrer</button></a>
        </div>
        <div class="col-md-4">
          <div class="card" style="margin-bottom :10px;">
            <div class="card-header">
              <h4 class="text-center">Mode 2 joueurs</h4>
            </div>
            <div class="card-body">
              <!-- Contenu de la carte pour le mode 2 joueurs -->
              <img src="b.png" alt="">
            </div>
          </div>
          <a href="#"><button type="button" class="btn btn-outline-danger mb-2" >Démarrer</button></a>
        </div>
        <div class="col-md-4">
          <div class="card" style="margin-bottom:10px;">
            <div class="card-header">
              <h4 class="text-center">Multijoueur en réseau</h4>
            </div>
            <div class="card-body">
              <!-- Contenu de la carte pour le mode en ligne -->
              <img src="c.png" alt="">
            </div>
          </div>
          <a href="#"><button type="button" class="btn btn-outline-danger mb-2" >Démarrer</button></a>
        </div>
    </div>
</div><br><br><br><br>
<?php 
  include 'footer.inc.php';
?>