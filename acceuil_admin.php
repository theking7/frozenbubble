<?php
session_start();
// Vérifier si l'utilisateur est authentifié
if (!isset($_SESSION['auth']['admin']) || $_SESSION['auth']['admin'] !== true) {
    // Rediriger vers une page d'erreur d'accès non autorisé'
    $_SESSION['err']="Accès non autorisée à cette page !!!";
    header('Location: erreur.php');
    exit;
}
else {
 echo '<?php';
       $titre = "Accueil | Admin";
       include 'header.inc.php';
       include 'menu_admin.php';
       include 'message.php';  
        echo '<br><br>';
        echo "<div class='container mx-auto text-center'>";
        echo "Bienvenue sur votre espace ". "<strong>" . $_SESSION['tonNom'] . "</strong>";
        echo "</div>";
        echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";

}
include 'footer.inc.php';
?>