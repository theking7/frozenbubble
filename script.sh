# #! /bin/bash
# #server
# sudo_password="portugal"
# #apache2 stopping
# echo -n "$sudo_password" | ssh portugal@195.221.60.129
# #lancer
# /usr/lib/games/frozen-bubble/fb-server -a pt -n frozen.pt -l 

# Variables d'authentification SSH
host="195.221.60.129"
username="portugal"
password="portugal"

# Commande à exécuter
commande="/usr/lib/games/frozen-bubble/fb-server -a pt -n frozen.pt -l"

# Connexion SSH et exécution de la commande
sshpass -p "$password" ssh "$username"@"$host" "$commande"

echo "Réusie"