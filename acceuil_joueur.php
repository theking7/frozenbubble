<?php
session_start();
// Vérifier si l'utilisateur est authentifié
if (!isset($_SESSION['auth']['joueur']) || $_SESSION['auth']['joueur'] !== true) {
  // Rediriger vers une page d'erreur d'accès non autorisé
  header('Location: erreur.php');
  exit;
}
else {
    # code...
    echo '<?php';
    $titre = "Accueil | Joueur";
    include 'header.inc.php';
    include 'menu_joueur.inc.php';
    include 'message.php';
    echo '<br><br>';
    echo "<div class='container mx-auto text-center'>";
    echo "Bienvenue sur votre espace ". "<strong>" . $_SESSION['tonNom'] . "</strong>";
    echo "</div>";
    echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
    echo '?>';
    include 'footer.inc.php';
}
?>