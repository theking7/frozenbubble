<!DOCTYPE html>
<html>
<head>
    <title>État des serveurs</title>
    <style>
        .server {
            margin-bottom: 10px;
        }

        .server .name {
            font-weight: bold;
            display: inline-block;
            width: 150px;
        }

        .server .status {
            display: inline-block;
            width: 10px;
            height: 10px;
            border-radius: 50%;
            margin-right: 5px;
        }

        .server .status.active {
            background-color: green;
        }

        .server .status.inactive {
            background-color: red;
        }
    </style>
</head>
<body>
    <?php
    session_start();
    if (!isset($_SESSION['auth']['organisateur']) || $_SESSION['auth']['organisateur'] !== true) {
        // Rediriger vers une page d'erreur d'accès non autorisé
        header('Location: erreur.php');
        exit;
      }
      else {
        # code...
        include 'header.inc.php';
    include 'menu_organisateur.inc.php';
    // Liste des serveurs à vérifier
    $servers = array(
        array('name' => 'Serveur Portugal', 'host' => '195.221.60.129', 'port' => 1511),
        array('name' => 'Serveur France', 'host' => '195.221.10.20', 'port' => 1511),
        array('name' => 'Serveur Chine', 'host' => '195.221.50.25', 'port' => 1511),
        array('name' => 'Serveur Espagne', 'host' => '195.221.20.140', 'port' => 1511),
        array('name' => 'Serveur Canada', 'host' => '195.221.30.66', 'port' => 1511)
    );

    // Vérification de l'état de chaque serveur
    foreach ($servers as $server) {
        $host = $server['host'];
        $port = $server['port'];

        // Tentative de connexion avec fsockopen
        $socket = @fsockopen($host, $port, $errorCode, $errorString, 1);

        echo '<div class="server">';
        echo '<span class="name">' . $server['name'] . '</span>';

        if ($socket) {
            echo '<span class="status active"></span> Actif';
            fclose($socket);
        } else {
            echo '<span class="status inactive"></span> Inactif';
        }

        echo '</div>';
    }
}
?>
</body>
</html>
