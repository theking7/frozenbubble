<?php
include 'menu_organisateur.inc.php';
include 'header.inc.php';
// Configuration du service SSH 
$sshHost = '195.221.60.132';
$sshPort = 22;
$sshUser = 'pi';
$sshPassword = 'Portugal';

// Command pour lancer une partie
$startCommand = 'killall frozen-bubble';

// Function qui permet l'exécution de sudo
function executeSshCommandWithSudo($host, $port, $user, $password, $command) {
    // Connexion à SSH
    #$connection = ssh2_connect($host, $port);
    $connection = ssh2_connect($host,$port);

    if (!$connection) {
        die('Failed to connect to SSH');
    }
    // Authentification avec SSH
    if (!ssh2_auth_password($connection, $user, $password)) {
        die('SSH authentication failed');
    }
    // Démarrer un terminal
    $shell = ssh2_shell($connection, 'xterm');
    if (!$shell) {
        die('Failed to start shell');
    }
    // Executer la commande voulue
    fwrite($shell, "export DISPLAY=:0\n");
    // Executer la commande voulue
    fwrite($shell, "xhost +local:\n");
    // Executer la commande voulue
    fwrite($shell, "$command\n");
    // Attendre la fin de l'exécution de la commande
    usleep(500000); // Wait for 500 milliseconds
    // Lecture de la sortie de cette commande
    $output = '';
    while ($line = fgets($shell)) {
        $output .= $line;
    }
    // Fermeture de la connection au SSH 
    ssh2_disconnect($connection);
    return $output;
}
// Execution de la commande avec sudo
$output = executeSshCommandWithSudo($sshHost, $sshPort, $sshUser, $sshPassword, $startCommand);
// Output the result
?>
<!DOCTYPE html>
<html>
<body>
<div class="container text-center">
        <h1>partie arretée avec succès.</h1>
    </div>
</body>
</html>