<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
$titre = "Connexion";
include 'header.inc.php';
include 'menu_conn.inc.php'; 
//
if (isset($_GET["test"])=='id') {
  # code...
    echo "<div class='alert alert-primary alert-dismissible fade show' role='alert'>";
    echo "<strong> Mauvais identifiant ou mot de passe !</strong>";
    echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>";
    echo "</button>";
    echo "</div>";
}
//
if (isset($_GET["message"])=='deco') {
  # code...
    echo "<div class='alert alert-primary alert-dismissible fade show' role='alert'>";
    echo "<strong> Déconnexion réussie !</strong>";
    echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>";
    echo "</button>";
    echo "</div>";
}
?>
<div class="mx-auto">
    <h2 class="text-center mt-2" style="font-family: 'Jost', sans-serif;">CONNEXION</h2>
</div>

<div class="container ">
<section class="h-100 gradient-form ">
    <div class="container py-5 h-50 ">
      <div class="row d-flex justify--center align-items-center h-100 ">
        <div class="col-xl-10">
          <div class="card rounded-3 text-black ">
            <div class="row g-0">
              <div class="col-lg-6">
                <div class="card-body p-md-5 mx-md-4 ">
                  <form method="POST" action="config.php">
                    <p>Entrez vos identifiants de connexion </p>
                    <div class="form-outline mb-4">
                      <input type="text" id="form2Example11" class="form-control" placeholder="Entrez votre identifiant " name="username" required/>
                    </div>
                    <div class="form-outline mb-4">
                      <input type="password" id="form2Example22" class="form-control" placeholder="Entrez votre mot de passe " name="pass" required/>
                    </div>
                    <div class="text-center pt-1 mb-5 pb-1">
                      <input class="btn btn-danger w-100" type="submit" value="S'AUTHENTIFIER">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php 
  include 'footer.inc.php';
?> 