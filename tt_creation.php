<?php
session_start();
// Informations de connexion Active Directory
$ldapServer = 'ldap://195.221.60.5';
$ldapPort = 389;
$ldapUser = 'franck@frozen.pt';
$ldapPassword = 'Password12';
$ldapBaseDN = 'OU=Joueurs,DC=frozen,DC=pt';

// Récupère les valeurs du formulaire
    if( !empty($_POST['le_prenom']) && !empty($_POST['le_user']) && !empty($_POST['le_pass']) && !empty($_POST['selectedOption'])){
        $prenom = $_POST['le_prenom'];
        $nom = $_POST['le_user'];
        $mdp = $_POST['le_pass'];
        
        if (isset($_POST['selectedOption'])) {
            $userType = $_POST['selectedOption'];
        }
         // Connexion à l'annuaire Active Directory
        $ldapConnection = ldap_connect($ldapServer, $ldapPort);
        ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTDIR, '/path/to/ca/bundle');

        // Modification de l'attribut "userAccountControl" pour activer le compte
        if ($ldapConnection) {
        // Authentification avec les informations de connexion
        $ldapBind = ldap_bind($ldapConnection, $ldapUser, $ldapPassword);

        if ($ldapBind) {
            // Crée un tableau d'attributs pour la nouvelle entrée
            $attributes = [
                'objectClass' => 'user',
                'givenName' => $prenom,
                'sn' => $nom,
                'sAMAccountName' => $prenom,
                'userPrincipalName' => $nom . '@frozen.pt',
                'userPassword' => $mdp,
                'description' => $userType,
            ];
            // Ajoute la nouvelle entrée à l'annuaire
            $result = ldap_add($ldapConnection, 'cn=' . $prenom .  ',' . $ldapBaseDN, $attributes);

            if ($result) {
                header('Location: add_user.php');
            } 
            else {
                echo "Erreur d'ajout de l'utilisateur dans l'AD ";
            }
        } else {
            echo 'Échec de l\'authentification avec l\'annuaire Active Directory.';
        }
        // Ferme la connexion LDAP
        ldap_unbind($ldapConnection);

    } else {
        echo 'Impossible de se connecter à l\'annuaire Active Directory.';
    }
    }
?>